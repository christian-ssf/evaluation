package com.main.java.mx.gemit.daos.daosImpl;

import com.main.java.mx.gemit.daos.CustomUserDAO;
import com.main.java.mx.gemit.daos.UserDAO;
import com.main.java.mx.gemit.entities.User;
import com.main.java.mx.gemit.entities.dtos.UserTypeDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */

public class UserDAOImpl extends GenericDataAccessImpl<User, UUID> implements CustomUserDAO {


    @Override
    public List<UserTypeDTO> getUsersAuth(User user) {
        return null;
    }
}
