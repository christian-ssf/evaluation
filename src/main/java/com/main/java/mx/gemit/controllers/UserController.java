package com.main.java.mx.gemit.controllers;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.main.java.mx.gemit.beans.RoleBean;
import com.main.java.mx.gemit.beans.UserBean;
import com.main.java.mx.gemit.beans.beanList.RoleBeanList;
import com.main.java.mx.gemit.beans.beanList.UserBeanList;
import com.main.java.mx.gemit.controllers.exceptions.CustomFailedException;
import com.main.java.mx.gemit.entities.Role;
import com.main.java.mx.gemit.entities.User;
import com.main.java.mx.gemit.services.RoleService;
import com.main.java.mx.gemit.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */

@RestController
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = UserBean.class),
        @JsonSubTypes.Type(value = UserBeanList.class)
})
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    /**
     {
     "email":"email_pruebo.com",
     "password":"123",
     "roleId":"030ad5ec-13de-419e-a40e-2e902661a0ae"
     }
     * @param userBean
     * @return
     */
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/createUser", method = RequestMethod.POST, headers = "Content-Type=application/json")
    @ResponseBody
    public UserBean createUser(@RequestBody UserBean userBean){
        userService.saveUser(populateUser(userBean, new User()));
        return userBean;
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/createUsers", method = RequestMethod.POST, headers = "Content-Type=application/json")
    @ResponseBody
    public UserBeanList createUsers(@RequestBody UserBeanList userBeanList){
        List<User> userList = new ArrayList<>();
        for (UserBean userBean : userBeanList.getUserBeanList()) {
            userList.add(populateUser(userBean, new User()));
        }
        userService.saveUserList(userList);
        return userBeanList;
    }

    /**
     {
     "email":"email_pruebamodifica11a",
     "password":"1234",
     "roleId":"030ad5ec-13de-419e-a40e-2e902661a0ae"
     }
     * @param userBean
     * @param userId
     * @return
     */
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/modifyUser/{userId}", method = RequestMethod.POST, headers = "Content-Type=application/json")
    @ResponseBody
    public UserBean modifyUser(@RequestBody UserBean userBean, @PathVariable UUID userId){
        User user = userService.getUserById(userId);
        if (user == null) {
            throw new CustomFailedException("El usuario a modificar no existe");
        }
        userService.saveUser(populateUser(userBean, user));
        return userBean;
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getUser/{userId}", method = RequestMethod.GET)
    public @ResponseBody UserBean getUser(@PathVariable UUID userId){
        User user = userService.getUserById(userId);
        if (user == null) {
            throw new CustomFailedException("El usuario a modificar no existe");
        }
        return new UserBean(user.getId(), user.getEmail(), user.getPassword(), user.getRole().getId());
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    public @ResponseBody UserBeanList getUsers(){
        List<User> userList = userService.getAllUsers();
        if (userList == null) {
            throw new CustomFailedException("El usuario a modificar no existe");
        }
        List<UserBean> userBeanList = new ArrayList<UserBean>();
        for (User user : userList) {
            userBeanList.add(new UserBean(user.getId(), user.getEmail(), user.getPassword(), user.getRole().getId()));
        }
        return new UserBeanList(userBeanList);
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/deleteUser/{userId}", method = RequestMethod.GET)
    public @ResponseBody UserBean deleteUser(@PathVariable UUID userId){
        User user = userService.getUserById(userId);
        if (user == null) {
            throw new CustomFailedException("El usuario a eliminar no existe");
        }
        userService.deleteUser(user);
        return new UserBean(user.getId(), user.getEmail(), user.getPassword(), user.getRole().getId());
    }


    private User populateUser(UserBean userBean, User user){
        if (user.getId() == null) {
            userBean.setId(UUID.randomUUID());
            user.setId(userBean.getId());
        }
        user.setEmail(userBean.getEmail());
        user.setPassword(userBean.getPassword());
        user.setRole(getRole(userBean.getRoleId()));
        return user;
    }

    private Role getRole(UUID roleId){
        Role role = roleService.getRoleById(roleId);
        if (role == null) {
            throw new CustomFailedException("No existe rol con el id: " + roleId);
        }
        return role;
    }
}
