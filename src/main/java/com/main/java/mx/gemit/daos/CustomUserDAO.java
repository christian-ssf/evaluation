package com.main.java.mx.gemit.daos;

import com.main.java.mx.gemit.entities.User;
import com.main.java.mx.gemit.entities.dtos.UserTypeDTO;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by christian on 18/12/16.
 */
public interface CustomUserDAO {

    public List<UserTypeDTO> getUsersAuth(User user);
}
