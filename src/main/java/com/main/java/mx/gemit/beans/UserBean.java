package com.main.java.mx.gemit.beans;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */
public class UserBean {


    private UUID id;

    @NotNull(message = "No se encontró la propiedad email")
    @Email(message = "El correo que se ingreso no es válido")
    private String email;

    @NotNull(message = "No se encontró la propiedad password")
    @Size(min = 6, max = 20, message = "El password debe tener una longitud de 6 a 20 caracteres")
    private String password;

    @NotNull(message = "El usuario debe tener un rol")
    private UUID roleId;

    public UserBean () {

    }

    public UserBean (UUID id, String email, String password, UUID roleId) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.roleId = roleId;
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UUID getRoleId() {
        return roleId;
    }

    public void setRoleId(UUID roleId) {
        this.roleId = roleId;
    }
}
