package com.main.java.mx.gemit.controllers;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.main.java.mx.gemit.beans.RoleBean;
import com.main.java.mx.gemit.beans.UserBean;
import com.main.java.mx.gemit.beans.beanList.RoleBeanList;
import com.main.java.mx.gemit.controllers.exceptions.CustomFailedException;
import com.main.java.mx.gemit.entities.Role;
import com.main.java.mx.gemit.entities.User;
import com.main.java.mx.gemit.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */

@RestController
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = RoleBean.class),
        @JsonSubTypes.Type(value = RoleBeanList.class)
})
public class RoleController {

    @Autowired
    private RoleService roleService;


    /** JSON To CREATE A NEW ROLE
     {
     "description":"Este es un administrador",
     "type":"ADMINISTRATOR"
     }
     * @param roleBean
     * @return
     */

    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/createRole", method = RequestMethod.POST, headers = "Content-Type=application/json")
    @ResponseBody
    public RoleBean createRole(@RequestBody RoleBean roleBean){
      roleService.saveRole(populateRole(roleBean, new Role()));
      return roleBean;
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/createRoles", method = RequestMethod.POST, headers = "Content-Type=application/json")
    @ResponseBody
    public RoleBeanList createRoles(@RequestBody RoleBeanList roleBeanList){
        List<Role> roleList = new ArrayList<>();
        for (RoleBean roleBean : roleBeanList.getRoleBeanList()) {
            roleList.add(populateRole(roleBean, new Role()));
        }
        roleService.saveRoleList(roleList);
        return roleBeanList;
    }
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getRole/{roleId}", method = RequestMethod.GET)
    public @ResponseBody RoleBean getRole(@PathVariable UUID roleId){
        Role role = roleService.getRoleById(roleId);
        if (role == null) {
            throw new CustomFailedException("El usuario a modificar no existe");
        }
        return new RoleBean(role.getId(), role.getDescription(), role.getType());
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getRoles", method = RequestMethod.GET)
    public @ResponseBody RoleBeanList getRoles(){
        List<Role> roleList = roleService.getAllRoles();
        if (roleList == null) {
            throw new CustomFailedException("El usuario a modificar no existe");
        }
        List<RoleBean> roleBeanList = new ArrayList<RoleBean>();
        for (Role role : roleList) {
            roleBeanList.add(new RoleBean(role.getId(), role.getDescription(), role.getType()));
        }
        return new RoleBeanList(roleBeanList);
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/modifyRole/{roleId}", method = RequestMethod.POST, headers = "Content-Type=application/json")
    @ResponseBody
    public RoleBean modifyUser(@RequestBody RoleBean roleBean, @PathVariable UUID roleId){
        Role role = roleService.getRoleById(roleId);
        if (role == null) {
            throw new CustomFailedException("El rol a modificar no existe");
        }
        roleService.saveRole(populateRole(roleBean, role));
        return roleBean;
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/deleteRole/{roleId}", method = RequestMethod.GET)
    public @ResponseBody RoleBean deleteUser(@PathVariable UUID roleId){
        Role role = roleService.getRoleById(roleId);
        if (role == null) {
            throw new CustomFailedException("El rol a eliminar no existe");
        }
        roleService.deleteRole(role);
        return new RoleBean(role.getId(), role.getDescription(), role.getType());
    }

    private Role populateRole(RoleBean roleBean, Role role){
        if (roleBean.getId() == null) {
            roleBean.setId(UUID.randomUUID());
            role.setId(roleBean.getId());
        }
        System.out.println("datos del bean: " + roleBean.getDescription() + ", " + roleBean.getType());
        role.setDescription(roleBean.getDescription());
        role.setType(roleBean.getType());
        return role;
    }
}
