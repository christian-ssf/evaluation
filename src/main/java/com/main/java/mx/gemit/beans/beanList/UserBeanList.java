package com.main.java.mx.gemit.beans.beanList;

import com.main.java.mx.gemit.beans.UserBean;

import java.util.List;

/**
 * Created by christian on 17/12/16.
 */
public class UserBeanList {
    private List<UserBean> userBeanList;

    public UserBeanList (List<UserBean> userBeanList) {
        this.userBeanList = userBeanList;
    }

    public List<UserBean> getUserBeanList() {
        return userBeanList;
    }

    public void setUserBeanList(List<UserBean> userBeanList) {
        this.userBeanList = userBeanList;
    }
}
