package com.main.java.mx.gemit.beans.beanList;

import com.main.java.mx.gemit.beans.RoleBean;

import java.util.List;

/**
 * Created by christian on 17/12/16.
 */
public class RoleBeanList {

    private List<RoleBean> roleBeanList;

    public RoleBeanList(){

    }

    public RoleBeanList (List<RoleBean> roleBeanList) {
        this.roleBeanList = roleBeanList;
    }

    public List<RoleBean> getRoleBeanList() {
        return roleBeanList;
    }

    public void setRoleBeanList(List<RoleBean> roleBeanList) {
        this.roleBeanList = roleBeanList;
    }
}
