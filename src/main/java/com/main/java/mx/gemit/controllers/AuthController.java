package com.main.java.mx.gemit.controllers;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.main.java.mx.gemit.beans.RoleBean;
import com.main.java.mx.gemit.beans.UserAuthBean;
import com.main.java.mx.gemit.beans.UserBean;
import com.main.java.mx.gemit.beans.beanList.RoleBeanList;
import com.main.java.mx.gemit.controllers.exceptions.CustomFailedException;
import com.main.java.mx.gemit.entities.Role;
import com.main.java.mx.gemit.entities.User;
import com.main.java.mx.gemit.entities.dtos.UserTypeDTO;
import com.main.java.mx.gemit.services.AuthService;
import com.main.java.mx.gemit.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by christian on 18/12/16.
 */

@RestController
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = RoleBean.class),
        @JsonSubTypes.Type(value = RoleBeanList.class),
        @JsonSubTypes.Type(value = UserAuthBean.class)
})
public class AuthController {

    @Autowired
    private AuthService authService;

    @Autowired
    private UserService userService;


    /**
     {
        "email":"email_pruebo.com",
         "password":"123",
     }
     * @param userAuthBean
     * @return
     */
    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/auth", method = RequestMethod.POST, headers = "Content-Type=application/json")
    @ResponseBody
    public UserTypeDTO auth(@RequestBody UserAuthBean userAuthBean){
        boolean exists = userService.getUserByEmailAndPassword(userAuthBean.getEmail(), userAuthBean.getPassword());
        if (!exists) {
            throw new CustomFailedException("El usuario no existe");
        }
        return authService.autenticate(userAuthBean.getEmail(), userAuthBean.getPassword());
    }
}
