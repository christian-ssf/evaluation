app.controller('usersController', function($scope, $http) {
	    $scope.headingTitle = "User Operations";

	    $scope.createUser = function(){
                    var jsonToSend = '{ "email":"' + $scope.email + '", "password":"' + $scope.password + '", "roleId":"' + $scope.uroleId + '"   }';
                    console.log("jsonToSend: " + jsonToSend);
                    var res = $http.post("http://localhost:8181/createUser", jsonToSend);
                        res.success(function(data, status, headers, config){
                          $scope.message = data;
                          alert("La insercion se ha realizado correctamente: " + angular.toJson(data));
                        });
                        res.error(function(data, status, headers, config){
                           alert("Fallo el envio de información, verifique que roleId si exista, json: " + angular.toJson(data));
                        });
        	    };

        $scope.getUsers = function() {
        	        $http.get('http://localhost:8181/getUsers').success(function(data, status, headers, config){
        	                      $scope.users = data;
                    	          console.log("datos: " + angular.toJson(data))
                                }).error(function(error) {
                                  console.log("Error al conseguir los usuarios");
                                });
        	    };

        $scope.deleteUser = function(id) {
        	        console.log("id: " + id);
                    $http.get('http://localhost:8181//deleteUser/'+id).success(function(data, status, headers, config){
                       console.log("Se ha eliminado el usuario con id: " + id);
                       alert("Se ha eliminado el usuario con id: " + id)
                       var lastItem = $scope.users.userBeanList.length-1;
                       $scope.users.userBeanList.splice(lastItem);
                    }).error(function(error){
                        console.log("Error al eliminar usuario");
                    });
                };

        $scope.editUser = function(id, email, password, roleId) {
                	        console.log("id: " + id);
                	        var jsonBean = '{"id":"' + id + '", "email":"' + email + '", "password":"' + password + '", "roleId":"' + roleId + '"}';
                            $http.post('http://localhost:8181/modifyUser/'+id, jsonBean).success(function(data, status, headers, config){
                               console.log("Se ha editado el usuario con id: " + id);
                               alert("Se ha editado el usuario con id: " + id)
                            }).error(function(error){
                                console.log("Error al editar usuario");
                            });
        	    };

	});

app.controller('rolesController', function($scope, $http, $timeout) {
	    $scope.headingTitle = "Roles Operations";

	    $scope.createRole = function(){
            var jsonToSend = '{ "description":"' + $scope.description + '", "type":"' + $scope.type + '"   }';
            console.log("jsonToSend: " + jsonToSend);
            var res = $http.post("http://localhost:8181/createRole", jsonToSend);
                res.success(function(data, status, headers, config){
                  $scope.message = data;
                  alert("La insercion se ha realizado correctamente: " + angular.toJson(data));
                });
                res.error(function(data, status, headers, config){
                   alert("Fallo el envio de información, json: " + angular.toJson(data));
                });
	    };

	    $scope.getRoles = function() {
	        $http.get('http://localhost:8181/getRoles').success(function(data, status, headers, config){
	                      $scope.roles = data;
            	          console.log("datos: " + angular.toJson(data))
                        }).error(function(error) {
                          console.log("Error al conseguir los roles");
                        });
	    };

	    $scope.deleteRole = function(id) {
	        console.log("id: " + id);
            $http.get('http://localhost:8181//deleteRole/'+id).success(function(data, status, headers, config){
               console.log("Se ha eliminado el rol con id: " + id);
               alert("Se ha eliminado el rol con id: " + id)
               var lastItem = $scope.roles.roleBeanList.length-1;
               $scope.roles.roleBeanList.splice(lastItem);
            }).error(function(error){
                alert("Fallo el intento de eliminar el rol, puede que este este asociado a un usuario")
                console.log("Error al eliminar rol");
            });
        };

        $scope.editRole = function(id, description, type) {
        	        console.log("id: " + id);
        	        var jsonBean = '{"id":"' + id + '", "description":"' + description + '", "type":"' + type + '"}';
                    $http.post('http://localhost:8181/modifyRole/'+id, jsonBean).success(function(data, status, headers, config){
                       console.log("Se ha editado el rol con id: " + id);
                       alert("Se ha editado el rol con id: " + id)
                    }).error(function(error){
                        console.log("Error al editar rol");
                    });
	    };
	});

app.controller('authController', function($scope, $http, $timeout) {
    $scope.headingTitle = "Login User";
    $scope.auth = function(email, password) {
        var jsonBean = '{"email":"' + email + '", "password":"' + password + '"}';
                    $http.post('http://localhost:8181/auth', jsonBean).success(function(data, status, headers, config){
                       alert("Has ingresado como " + data.role +  ". Bienvenido: " + data.email)
                    }).error(function(error){
                        console.log("El usuario que intenta ingresar no existe");
                        alert("El usuario que intenta ingresar no existe");
                    });
    };
});