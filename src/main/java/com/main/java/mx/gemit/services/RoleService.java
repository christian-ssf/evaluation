package com.main.java.mx.gemit.services;

import com.main.java.mx.gemit.entities.Role;
import org.jvnet.hk2.annotations.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */
@Service
@Transactional
public interface RoleService {

    public Role getRoleById(UUID roleId);

    public Role saveRole(Role role);

    public void saveRoleList(List<Role> roleList);

    public void deleteRole(Role role);

    public List<Role> getAllRoles();
}
