package com.main.java.mx.gemit.services.servicesImpl;

import com.main.java.mx.gemit.daos.RoleDAO;
import com.main.java.mx.gemit.entities.Role;
import com.main.java.mx.gemit.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */
@Service("RoleService")
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDAO roleDAO;

    @Override
    public Role getRoleById(UUID roleId) {
        return roleDAO.findOne(roleId);
    }

    @Override
    public Role saveRole(Role role) {
        return roleDAO.save(role);
    }

    @Override
    public void saveRoleList(List<Role> roleList) {
        roleDAO.save(roleList);
    }

    @Override
    public void deleteRole(Role role) {
        roleDAO.delete(role);
    }

    @Override
    public List<Role> getAllRoles() {
        return roleDAO.findAll();
    }
}
