package com.main.java.mx.gemit.beans;

/**
 * Created by christian on 18/12/16.
 */
public class UserAuthBean {

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
