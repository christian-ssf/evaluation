package com.main.java.mx.gemit.daos;

import com.main.java.mx.gemit.entities.User;
import com.main.java.mx.gemit.entities.dtos.UserTypeDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */

public interface UserDAO extends JpaRepository<User, UUID>, JpaSpecificationExecutor<User>, CustomUserDAO {

    @Query("SELECT new com.main.java.mx.gemit.entities.dtos.UserTypeDTO(u.email, u.role.type) FROM User u WHERE u.email = :email AND u.password = :password")
    UserTypeDTO getUserAuth(@Param("email") String email, @Param("password") String password);

    @Query("SELECT count(u) FROM User u WHERE u.email = :email AND u.password = :password")
    int existsUserByEmailAndPassword(@Param("email") String email, @Param("password") String password);
}
