package com.main.java.mx.gemit.entities;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.StringType;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
/**
 * Created by christian on 17/12/16.
 */
public class UUIDJPAType implements UserType, Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public Object assemble(Serializable cached, Object owner)
            throws HibernateException {
        return cached;
    }

    @Override
    public Object deepCopy(Object object) throws HibernateException {
        return UUID.fromString(object.toString());
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return x != null && x.equals(y);
    }

    @Override
    public int hashCode(Object value) throws HibernateException {
        assert (value != null);
        return value.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] name, SessionImplementor session, Object o) throws HibernateException, SQLException {
        UUID gettedUUID;

        String stringUUID = (String) StringType.INSTANCE.get(rs, name[0], session);
        if (stringUUID == null || stringUUID.isEmpty()) {
            return null;
        }
        gettedUUID = UUID.fromString(stringUUID);
        return gettedUUID;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
        if (value == null) {
            StringType.INSTANCE.set(st, null, index, session);
            return;
        }
        UUID uuid = (UUID) value;
        StringType.INSTANCE.set(st, uuid.toString(), index, session);
    }

    @Override
    public boolean isMutable() {
        return false;
    }
/*
  @Override
	public Object nullSafeGet(ResultSet rs, String[] name,
                            SharedSessionContractImplementor session, Object owner)
			throws HibernateException, SQLException {
		UUID gettedUUID;

		String stringUUID = (String) StringType.INSTANCE.get(rs, name[0], session);
		if (stringUUID == null || stringUUID.isEmpty()) {
		    return null;
		}
		gettedUUID = UUID.fromString(stringUUID);
		return gettedUUID;
	}

  @Override
	public void nullSafeSet(PreparedStatement st, Object value, int index,
                          SharedSessionContractImplementor session) throws HibernateException, SQLException {
		if (value == null) {
			StringType.INSTANCE.set(st, null, index, session);
			return;
		}
		UUID uuid = (UUID) value;
		StringType.INSTANCE.set(st, uuid.toString(), index, session);
	}*/

    @Override
    public Object replace(Object original, Object target, Object owner)
            throws HibernateException {
        return original;
    }

    @Override
    public Class<UUID> returnedClass() {
        return UUID.class;
    }

    @Override
    public int[] sqlTypes() {
        return new int[] { StringType.INSTANCE.sqlType() };
    }

}
