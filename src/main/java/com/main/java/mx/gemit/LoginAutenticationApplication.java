package com.main.java.mx.gemit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@EnableJpaRepositories("com.main.java.mx.gemit.daos")
@EnableTransactionManagement
@ComponentScan({
		"com.main.java.mx.gemit.entities",
		"com.main.java.mx.gemit.services",
		"com.main.java.mx.gemit.controllers"})
@EntityScan("com.main.java.mx.gemit.entities")
@PropertySource("classpath:application.properties")
@SpringBootApplication
@RestController
public class LoginAutenticationApplication {
	public static void main(String[] args) {
		SpringApplication.run(LoginAutenticationApplication.class, args);
	}
}
