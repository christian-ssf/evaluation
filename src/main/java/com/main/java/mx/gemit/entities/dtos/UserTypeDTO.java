package com.main.java.mx.gemit.entities.dtos;

import com.main.java.mx.gemit.entities.Role;

/**
 * Created by christian on 17/12/16.
 */
public class UserTypeDTO {

    private String email;
    private Role.TYPE role;

    public UserTypeDTO(String email, Role.TYPE role){
        this.email = email;
        this.role = role;
    }

    public Role.TYPE getRole() {
        return role;
    }

    public void setRole(Role.TYPE role) {
        this.role = role;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
