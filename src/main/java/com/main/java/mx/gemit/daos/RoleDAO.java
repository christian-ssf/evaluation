package com.main.java.mx.gemit.daos;

import com.main.java.mx.gemit.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */

public interface RoleDAO extends JpaRepository<Role, UUID>, JpaSpecificationExecutor<Role> {
}
