package com.main.java.mx.gemit.daos.daosImpl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.main.java.mx.gemit.daos.GenericDataAccess;
import org.hibernate.jpa.internal.EntityManagerFactoryImpl;
import org.hibernate.stat.EntityStatistics;
import org.hibernate.stat.Statistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.orm.jpa.EntityManagerFactoryInfo;

/**
 * Created by christian on 17/12/16.
 */


public abstract class GenericDataAccessImpl<T, ID extends Serializable> implements GenericDataAccess<T, ID>, ApplicationContextAware {

    public static final String NORMAL_CACHE_MODE = "NORMAL";

    private Logger log = LoggerFactory.getLogger(GenericDataAccessImpl.class);

    private final Class<T> persistentClass;
    private EntityManager entityManager;

    private ApplicationContext appContext;

    @SuppressWarnings("unchecked")
    public GenericDataAccessImpl() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public GenericDataAccessImpl(final Class<T> persistentClass) {
        super();
        this.persistentClass = persistentClass;
    }

    /**
     *
     * @param query
     * @return
     */
    protected T getSafeSingleResult(TypedQuery<T> query) {
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }


    @Override
    public T findById(final ID id) {
        return getEntityManager().find(persistentClass, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        return entityManager.createQuery("Select t from " + persistentClass.getSimpleName() + " t").getResultList();
    }


    @Override
    public Class<T> getEntityClass() {
        return persistentClass;
    }

    /**
     * set the JPA entity manager to use.
     *
     * @param entityManager
     */
    @Required
    @PersistenceContext
    public void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }


    @Override
    public void delete(T entity) {
        getEntityManager().remove(entity);
    }


    @Override
    public T save(T entity) {
        final T savedEntity = getEntityManager().merge(entity);
        getEntityManager().flush();
        return savedEntity;
    }

    /**
     * @param batch
     */
    public void saveBatch(List<T> batch) {
        for (int i = 0; i < batch.size(); i++) {
            T entity = batch.get(i);
            getEntityManager().merge(entity);
            if (i % 20 == 0) {
                getEntityManager().flush();
                getEntityManager().clear();
            }
        }
    }

    @Override
    public void deleteBatch(List<T> batch) {
        for (int i = 0; i < batch.size(); i++) {
            T entity = batch.get(i);
            entity = getEntityManager().merge(entity);
            getEntityManager().remove(entity);
            if (i % 20 == 0) {
                getEntityManager().flush();
                getEntityManager().clear();
            }
        }
    }

    @Override
    public void insert(T entity) {
        getEntityManager().persist(entity);
        getEntityManager().flush();
    }

    @Override
    public void setApplicationContext(ApplicationContext appContext) throws BeansException {
        this.appContext = appContext;
    }

    /**
     *
     * @return
     */
    @Override
    public Statistics getGlobalStatistics() {
        if (appContext == null) {
            log.warn("Imposible obtener estadisticas. No fue posible obtener el applicationContext para generar el objeto Statistics");
            throw new RuntimeException("Imposible obtener estadisticas. No fue posible obtener el applicationContext para generar el objeto Statistics");
        }
        EntityManagerFactoryInfo entityManagerFactoryInfo = (EntityManagerFactoryInfo) appContext.getBean("entityManagerFactory");
        EntityManagerFactory emf = entityManagerFactoryInfo.getNativeEntityManagerFactory();
        EntityManagerFactoryImpl emfImp = (EntityManagerFactoryImpl) emf;
        return emfImp.getSessionFactory().getStatistics();
    }

    /**
     * @return
     */
    @Override
    public EntityStatistics getEntityStatistics() {
        Statistics globalStats = getGlobalStatistics();
        return globalStats.getEntityStatistics(this.persistentClass.getName());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * mx.ssf.sicom.commondomain.genericdao.GenericDataAccess#clearStatistics()
     */
    @Override
    public void clearStatistics() {
        getGlobalStatistics().clear();
    }

    @Override
    public T attach(T entity) {
        return getEntityManager().merge(entity);
    }

    /**
     * Sets the cache Mode to the given query object. The cache Mode is defined
     * by the org.hibernate.cacheMode property due Hibernate is the provider
     * that the application uses. To see the valid values of the cacheMode
     * parameter, please see the Hibernate documentation.
     *
     * @param query
     * @param cacheMode
     * @return
     */
    public Query setQueryCacheable(Query query, String cacheMode) {
        query.setHint("org.hibernate.cacheable", true);
        query.setHint("org.hibernate.cacheMode", cacheMode);
        return query;
    }
}

