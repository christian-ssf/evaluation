package com.main.java.mx.gemit.services.servicesImpl;

import com.main.java.mx.gemit.daos.UserDAO;
import com.main.java.mx.gemit.entities.User;
import com.main.java.mx.gemit.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */

@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public User saveUser(User user) {
        return userDAO.save(user);
    }

    @Override
    public void saveUserList(List<User> userList) {
        userDAO.save(userList);
    }

    @Override
    public User getUserById(UUID userId) {
        return userDAO.getOne(userId);
    }

    @Override
    public void deleteUser(User user) {
        userDAO.delete(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userDAO.findAll();
    }

    @Override
    public boolean getUserByEmailAndPassword(String email, String password) {
        return userDAO.existsUserByEmailAndPassword(email, password) > 0;
    }
}
