package com.main.java.mx.gemit.daos;

import java.io.Serializable;
import java.util.List;

import org.hibernate.stat.EntityStatistics;
import org.hibernate.stat.Statistics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by christian on 17/12/16.
 */


public interface GenericDataAccess<T, ID extends Serializable> {

    /**
     * Get the Class of the entity.
     *
     * @return the class
     */
    Class<T> getEntityClass();

    /**
     * Find an entity by its primary key
     *
     * @param id
     *            the primary key
     * @return the entity
     */
    T findById(final ID id);

    /**
     * Return a list of all the records in the database.
     *
     * @return
     */
    List<T> findAll();

    /**
     * save an entity. This can be either a INSERT or UPDATE in the database.
     *
     * @param entity
     *            the entity to save
     *
     * @return the saved entity
     */
    T save(final T entity);

    /**
     *
     * @param batch
     */
    void saveBatch(List<T> batch);

    /**
     * delete an entity from the database.
     *
     * @param entity
     *            the entity to delete
     */
    void delete(final T entity);

    /**
     * Saves a new instance of the given Entity in the database.
     *
     * @param entity
     * @return
     */
    void insert(final T entity);

    /**
     * Erase the hibernate statistics.
     */
    void clearStatistics();

    /**
     * Get the global statistics for the hibernate session
     *
     * @return
     */
    Statistics getGlobalStatistics();

    /**
     * Get the hibernate statistics of the actual data access object entity.
     *
     * @return
     */
    EntityStatistics getEntityStatistics();

    T attach(final T entity);

    /**
     *
     * @param batch
     */
    void deleteBatch(List<T> batch);
}

