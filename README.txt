## EVALUACION GRUPO ETYTE ##
## FECHA 18 DE DICIEMBRE DEL 2016 ##
## EVALUADO: CHRISTIAN OMAR LÓPEZ TREJO ##
## EMAIL DEL EVALUADO: christian.l.trejo@gmail.com ##
## ACERCA DE LA EVALUACION ##

## APLICACION ##

## PRE REQUISITOS LINUX ##

1. Maven 3
2. Postgresql 9.3 o posterior
3. Ingrese a la consola de postgresql desde una terminal: sudo su postgres
#NOTA# la terminal solicitará la contraseña del usuario con el que inicia sesion en Linux. Ingresela.
4. Ejecutar en consola de postgresql: CREATE ROLE gem WITH LOGIN ENCRYPTED PASSWORD 'gem';
5. Ejecutar en consola de postgresql: CREATE DATABASE gemit WITH OWNER gem;


## OPCIONES DE INSTALACION ##

1. Descomprimir el .zip que adjunté en el correo y compilarlo con Maven 3.
2. Clonar el Repositorio en Bitbucket, anexo la liga: git clone https://christian-ssf@bitbucket.org/christian-ssf/evaluation.git

## INICIAR APLICACION ##

## Linux ##

1. Ingrese a la ruta del proyecto cd ~/login_autenticaton/
2. Compile con maven, ejecutando el siguiente comando: mvn clean install. Verifique que la compilación haya creado las tablas roles y users en la base de datos gemit.
3. Arranque la aplicación con el comando maven siguiente: mvn spring-boot:run
##NOTA## En caso de que no arranque, ejecute el comando mvn package y posteriormente intente de nuevo arrancar la aplicación
4. Acceda a http://localhost:8181/

## BASE DE DATOS ##
Tablas: users y roles


## REGISTRO AUTOGENERADO EN ROLES ##

                  id                  |    description    |     type      
--------------------------------------+-------------------+---------------
 9ba21f20-ff81-44ac-9b0f-237e6453cfcd | Role autogenerado | ADMINISTRATOR

## REGISTRO AUTOGENERADO EN USERS ##

                  id                  | email | password |               role_id                
--------------------------------------+-------+----------+--------------------------------------
 e70be946-4e64-4d86-a1eb-fedfa786d528 | admin | admin    | 9ba21f20-ff81-44ac-9b0f-237e6453cfcd
 

## DUDAS O ACLARACIONES ##
Favor de contactarme en mi correo. Saludos.



