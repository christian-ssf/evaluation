package com.main.java.mx.gemit.services.servicesImpl;

import com.main.java.mx.gemit.daos.UserDAO;
import com.main.java.mx.gemit.entities.User;
import com.main.java.mx.gemit.entities.dtos.UserTypeDTO;
import com.main.java.mx.gemit.services.AuthService;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by christian on 18/12/16.
 */
@org.springframework.stereotype.Service("AuthService")
@Transactional
public class AuthServiceImpl implements AuthService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public UserTypeDTO autenticate(String email, String password) {
        return userDAO.getUserAuth(email,password);
    }
}
