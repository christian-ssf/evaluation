var app = angular.module('app', ['ngRoute','ngResource']);
	app.config(function($routeProvider){
	    $routeProvider
	        .when('/newRole',{
	            templateUrl: '/views/crearRol.html',
	            controller: 'rolesController'
	        })
            .when('/editRole',{
	            templateUrl: '/views/editarRol.html',
	            controller: 'rolesController'
	        })
            .when('/lookRole',{
	            templateUrl: '/views/verRol.html',
	            controller: 'rolesController'
	        })
	        .when('/newUser',{
	            templateUrl: '/views/crearUsuario.html',
                controller: 'usersController'
            })
            .when('/editUser',{
	            templateUrl: '/views/editarUsuario.html',
                controller: 'usersController'
            })
            .when('/lookUser',{
	            templateUrl: '/views/verUsuario.html',
                controller: 'usersController'
            })
            .when('/auth',{
	            templateUrl: '/views/login.html',
                controller: 'authController'
            })
	        .otherwise(
	            { redirectTo: '/'}
	        );
	});