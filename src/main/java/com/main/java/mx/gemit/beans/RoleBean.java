package com.main.java.mx.gemit.beans;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.main.java.mx.gemit.beans.beanList.RoleBeanList;
import com.main.java.mx.gemit.entities.Role;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */

public class RoleBean {

    private UUID id;

    @Size(min = 0, max = 255, message = "El password debe tener una longitud de 6 a 20 caracteres")
    private String description;

    @NotNull(message = "Ingrese un tipo de rol: ADMINISTRATOR o VIEWER")
    private Role.TYPE type;

    public RoleBean(){

    }

    public RoleBean(UUID id, String description, Role.TYPE type){
        this.id = id;
        this.description = description;
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Role.TYPE getType() {
        return type;
    }

    public void setType(Role.TYPE type) {
        this.type = type;
    }
}
