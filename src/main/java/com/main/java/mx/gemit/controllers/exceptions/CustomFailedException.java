package com.main.java.mx.gemit.controllers.exceptions;

/**
 * Created by christian on 17/12/16.
 */
public class CustomFailedException extends RuntimeException {
    public CustomFailedException (String message) {
        super(message);
    }
}
