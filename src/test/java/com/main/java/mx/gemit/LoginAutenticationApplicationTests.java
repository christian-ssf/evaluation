package com.main.java.mx.gemit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@RunWith(SpringRunner.class)
@EnableJpaRepositories("com.main.java.mx.gemit.daos")
@EnableTransactionManagement
@ComponentScan({"com.main.java.mx.gemit.entities", "com.main.java.mx.gemit.services", "com.main.java.mx.gemit.controllers"})
@EntityScan("com.main.java.mx.gemit.entities")
@PropertySource("classpath:application.properties")
@SpringBootTest
public class LoginAutenticationApplicationTests {

	@Test
	public void contextLoads() {
	}

}
