package com.main.java.mx.gemit.services;

import com.main.java.mx.gemit.entities.User;
import org.jvnet.hk2.annotations.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */

@Service
@Transactional
public interface UserService {

    public User saveUser(User user);

    public void saveUserList(List<User> userList);

    public User getUserById(UUID userId);

    public void deleteUser(User user);

    public List<User> getAllUsers();

    public boolean getUserByEmailAndPassword(String email, String password);
}
