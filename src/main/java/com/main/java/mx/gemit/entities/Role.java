package com.main.java.mx.gemit.entities;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

/**
 * Created by christian on 17/12/16.
 */

@Entity
@Table(name = "roles")
@TypeDef(name = "uuid", typeClass = UUIDJPAType.class)
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @Type(type = "pg-uuid")
    private UUID id;

    @Column(name = "description")
    private String description;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private TYPE type;

    public Role () {

    }
    public static enum TYPE {
        ADMINISTRATOR("ADMINISTRATOR"),
        VIEWER("VIEWER");
        private String description;
        TYPE(String description){
            this.description = description;
        }
        public String getDescription(){
            return description;
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }
}
