package com.main.java.mx.gemit.services;

import com.main.java.mx.gemit.entities.User;
import com.main.java.mx.gemit.entities.dtos.UserTypeDTO;
import org.jvnet.hk2.annotations.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by christian on 18/12/16.
 */
@Service
@Transactional
public interface AuthService {


    public UserTypeDTO autenticate(String email, String password);
}
